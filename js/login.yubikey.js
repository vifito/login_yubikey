/**
 * TODO: empregar Drupal behaviors
 */
;(function($) {
    $(document).ready(function() {
        let token = '';

        $('#user-login-form').submit(function(evt) {
            console.log('Submit');

            if($('[name="yubikey_token"]').val().length === 44) {
                return true;
            }

            evt.preventDefault();
            evt.stopPropagation();

            token = '';

            // Abrir o diálogo
            $('#yubikey_dialog').dialog({
                focus: function( event, ui ) {
                    // Establecer o foco
                    $('#yubikey_dialog p').focus();
                }
            });
        });

        $('#yubikey_dialog').keypress(function(evt) {
            if(evt.which == 13) {
                $('#yubikey_dialog').dialog('close');
                assignAndSubmit(token);
            }

            token += String.fromCharCode(evt.which);
        });

        $('#yubikey_dialog').click(function(evt) {
            console.log(evt);
        });
    });

    var assignAndSubmit = function(value) {
        $('[name="yubikey_token"]').val(value);
        $('#user-login-form').submit();
    };
})(jQuery);
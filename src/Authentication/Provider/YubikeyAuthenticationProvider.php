<?php

namespace Drupal\login_yubikey\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Password\PhpassHashedPassword;
use Drupal\user\UserDataInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

// use Drupal\user\Authentication\Provider\Cookie;

/**
 * Class YubikeyAuthenticationProvider.
 */
class YubikeyAuthenticationProvider implements AuthenticationProviderInterface
{

    /**
     * The config factory.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $configFactory;

    /**
     * The entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * Constructs a HTTP basic authentication provider object.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *   The config factory.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager service.
     */
    public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager)
    {
        $this->configFactory = $config_factory;
        $this->entityTypeManager = $entity_type_manager;
    }

    /**
     * Checks whether suitable authentication credentials are on the request.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   The request object.
     *
     * @return bool
     *   TRUE if authentication credentials suitable for this provider are on the
     *   request, FALSE otherwise.
     */
    public function applies(Request $request)
    {
        if(!$request->isMethod("POST")) {
            return FALSE;
        }

        $yubikeyToken = $request->request->get('yubikey_token', null);
        if ($yubikeyToken !== null) {
            return TRUE;
        }

        return FALSE;
    }

    protected function checkYubikeyToken()
    {
        // TODO
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(Request $request)
    {
        $logger = \Drupal::logger('login_yubikey');

        $yubikeyToken = $request->request->get('yubikey_token');

        $logger->notice('Request token: ' . $yubikeyToken);

        if(mb_strlen($yubikeyToken) === 44) {
            $name = $request->request->get('name');
            $pass = $request->request->get('pass');

            $logger->notice('Request name: ' . $name);
            $logger->notice('Request pass: ' . $pass);

            /** @var \Drupal\Core\Entity\EntityInterface[] $user */
            $result = $this->entityTypeManager->getStorage('user')->loadByProperties([
                'name' => $name,
            ]);

            if(count($result) !== 1) {
                $logger->warning('User not found: ' . $name );
                throw new AccessDeniedHttpException();
            }

            /** @var \Drupal\user\Entity\User $user */
            $user = array_shift($result);

            /** @var \Drupal\Core\Password\PhpassHashedPassword $passwordHasher */
            $passwordHasher = \Drupal::service('password');
            $isPassOk = $passwordHasher->check($pass, $user->getPassword());

            if(!$isPassOk) {
                $logger->warning('Password not match: ' . $user->getUsername() );
                throw new AccessDeniedHttpException();
            }
            $logger->warning('Password ok' );


            /* if($user->get('field_token')->isEmpty()) {
                $logger->warning('Token undefined: ' . $user->getUsername());
                throw new AccessDeniedHttpException();
            }
            $token = $user->get('field_token')[0]->value; */
            /** @var UserDataInterface $userData */
            $userData = \Drupal::service('user.data');
            $token = $userData->get('login_yubikey', $user->id(), 'yubikey_id');

            $logger->warning('Token request: ' . mb_substr($yubikeyToken, 0, 12));
            $logger->warning('Token db: ' . $token);

            if(mb_substr($yubikeyToken, 0, 12) !== $token) {
                $logger->warning('Token unowned: ' . $yubikeyToken );
                throw new AccessDeniedHttpException();
            }

            $logger->notice('authentication ok: ' . $user->getUsername() );

            return $user;
        } else {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function cleanup(Request $request)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function handleException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        if ($exception instanceof AccessDeniedHttpException) {
            $event->setException(
                new UnauthorizedHttpException('Invalid consumer origin.', $exception)
            );
            return TRUE;
        }
        return FALSE;
    }

}

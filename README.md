# Drupal module for use USB Yubikey token

Drupal module to use yubikey usb token

Configuración de composer.json

```

 "repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "package",
            "package": {
              "name": "vifito/login_yubikey",
              "type": "drupal-custom-module",
              "version": "master",
              "source": {
                "url": "https://vifito@gitlab.com/vifito/login_yubikey.git",
                "type": "git",
                "reference": "master"
              }
            }
        }
    ],

```
